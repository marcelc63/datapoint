# Data Point by Marcel Christianis

Data Point is a user friendly Data Scraping tool that helps in collecting information from digital platforms. Supported platforms and datasets will be updated periodically.

Data Point currently support Airbnb and Tokopedia data. More will be added periodically.

Data Point is built on top of Nuxt.js

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

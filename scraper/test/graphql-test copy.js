let axios = require('axios')

console.log('test')

axios({
  url: 'https://gql.tokopedia.com/',
  method: 'post',
  headers: {
    accept: '*/*',
    'accept-encoding': 'gzip, deflate, br',
    'accept-language': 'en-US,en;q=0.9,id;q=0.8,ms;q=0.7,pt;q=0.6',
    'content-type': 'application/json',
    origin: 'https://www.tokopedia.com',
    referer:
      'https://www.tokopedia.com/search?q=dispenser+galon+bawah&source=universe&st=product',
    'sec-fetch-dest': 'empty',
    'sec-fetch-mode': 'cors',
    'sec-fetch-site': 'same-site',
    'user-agent':
      'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36',
    'x-device': 'desktop-0.0',
    'x-source': 'tokopedia-lite',
    'x-tkpd-lite-service': 'zeus'
  },
  data: {
    operationName: 'SearchProductQueryV4',
    variables: {
      params:
        'scheme=https&device=desktop&related=true&st=product&q=xiaomi&ob=23&page=1&variants=&shipping=&start=0&rows=120&safe_search=false&source=search'
    },
    query: `
      query SearchProductQueryV4($params: String!) {
  ace_search_product_v4(params: $params) {
    header {
      totalData
      totalDataText
      processTime
      responseCode
      errorMessage
      additionalParams
      keywordProcess
      __typename
    }
    data {
      isQuerySafe
      ticker {
        text
        query
        typeId
        __typename
      }
      redirection {
        redirectUrl
        departmentId
        __typename
      }
      related {
        relatedKeyword
        otherRelated {
          keyword
          url
          __typename
        }
        __typename
      }
      suggestion {
        currentKeyword
        suggestion
        suggestionCount
        instead
        insteadCount
        query
        text
        __typename
      }
      products {
        id
        name
        ads {
          id
          productClickUrl
          productWishlistUrl
          productViewUrl
          __typename
        }
        badges {
          title
          imageUrl
          show
          __typename
        }
        category: departmentId
        categoryBreadcrumb
        categoryId
        categoryName
        countReview
        discountPercentage
        gaKey
        imageUrl
        labelGroups {
          position
          title
          type
          __typename
        }
        originalPrice
        price
        priceRange
        rating
        shop {
          id
          name
          url
          city
          isOfficial
          isPowerBadge
          __typename
        }
        url
        wishlist
        __typename
      }
      __typename
    }
    __typename
  }
}
      `
  }
})
  .then((result) => {
    // console.log(result.data.header)
    // console.log(result.data.data)
    console.log('TEST', result.data.data.ace_search_product_v4.data.products)
    console.log(
      'TEST',
      result.data.data.ace_search_product_v4.data.products.length
    )
  })
  .catch((error) => {
    console.log(error)
  })

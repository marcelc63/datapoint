let axios = require('axios')

console.log('test')

axios({
  url: 'https://gql.tokopedia.com/',
  method: 'post',
  headers: {
    accept: '*/*',
    'accept-encoding': 'gzip, deflate, br',
    'accept-language': 'en-US,en;q=0.9,id;q=0.8,ms;q=0.7,pt;q=0.6',
    'content-type': 'application/json',
    origin: 'https://www.tokopedia.com',
    referer:
      'https://www.tokopedia.com/search?q=dispenser+galon+bawah&source=universe&st=product',
    'sec-fetch-dest': 'empty',
    'sec-fetch-mode': 'cors',
    'sec-fetch-site': 'same-site',
    'user-agent':
      'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36',
    'x-device': 'desktop-0.0',
    'x-source': 'tokopedia-lite',
    'x-tkpd-lite-service': 'zeus'
  },
  data: {
    operationName: 'PDPReviewListQuery',
    variables: {
      page: 1,
      rating: 0,
      withAttachment: 0,
      productID: 720119046,
      perPage: 10
    },
    query: `
      query PDPReviewListQuery($productID: Int!, $page: Int!, $perPage: Int!, $rating: Int!, $withAttachment: Int!) {
  ProductReviewListQuery(productId: $productID, page: $page, perPage: $perPage, rating: $rating, withAttachment: $withAttachment) {
    shop {
      shopId
      name
      image
      url
      __typename
    }
    list {
      reviewId
      message
      productRating
      reviewCreateTime
      reviewCreateTimestamp
      isReportable
      isAnonymous
      imageAttachments {
        attachmentId
        imageUrl
        imageThumbnailUrl
        __typename
      }
      reviewResponse {
        message
        createTime
        __typename
      }
      likeDislike {
        totalLike
        likeStatus
        __typename
      }
      user {
        userId
        fullName
        image
        url
        __typename
      }
      __typename
    }
    __typename
  }
}
      `
  }
})
  .then((result) => {
    console.log(result.data)
    console.log(result.data.data)
    console.log('TEST', result.data.data.ProductReviewListQuery.list[0])
    console.log('TEST', result.data.data.ProductReviewListQuery.list.length)
  })
  .catch((error) => {
    console.log(error)
  })

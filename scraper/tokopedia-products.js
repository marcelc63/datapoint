import axios from 'axios'
import _ from 'lodash'

const cycleState = {
  status: 'stop'
}

async function wait(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms)
  })
}

export async function scrape(CONFIG, fnLog) {
  //Send Log
  fnLog({
    type: 'status',
    status: 'scraping'
  })

  //Define
  let QUERY = CONFIG.query.replace(' ', '%20')
  let OFFSET = 0

  let result = await cycle(QUERY, OFFSET, fnLog)
  console.log('Scrape complete')
  return {
    data: result.data,
    success: true
  }
}

async function cycle(QUERY, OFFSET, fnLog) {
  let result = await page(QUERY, OFFSET, fnLog)
  if (result.success) {
    if (cycleState.status === 'stop') {
      //Send Log
      fnLog({
        type: 'status',
        status: 'stopping'
      })
      return {
        data: result.data,
        success: true
      }
    }

    //Send Log
    fnLog({
      type: 'progress',
      progress: 1
    })

    if (result.data.length !== 0) {
      //Next Page Evaluator
      OFFSET = OFFSET + 200
      let data = await cycle(QUERY, OFFSET, fnLog)
      console.log('CYCLE RESOLVE DATA', data)
      return {
        data: result.data.concat(data.data),
        success: true
      }
    } else {
      //Complete
      console.log('DONE', result.data.length, OFFSET)
      cycleState.status = 'stop'
      // return result
      return {
        data: result.data,
        success: true
      }
    }
  } else {
    await wait(5000)
    let result = await cycle(CONFIG, fnLog)
    return {
      data: result.data,
      success: true
    }
  }
}

async function page(QUERY, OFFSET, fnLog) {
  try {
    //Request
    const response = await axios({
      url: 'https://gql.tokopedia.com/',
      method: 'post',
      headers: {
        accept: '*/*',
        // 'accept-encoding': 'gzip, deflate, br',
        'accept-language': 'en-US,en;q=0.9,id;q=0.8,ms;q=0.7,pt;q=0.6',
        'content-type': 'application/json',
        // origin: 'https://www.tokopedia.com',
        // 'sec-fetch-dest': 'empty',
        // 'sec-fetch-mode': 'cors',
        // 'sec-fetch-site': 'same-site',
        // 'user-agent':
        //   'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36',
        'x-device': 'desktop-0.0',
        'x-source': 'tokopedia-lite',
        'x-tkpd-lite-service': 'zeus'
      },
      data: {
        operationName: 'SearchProductQueryV4',
        variables: {
          params: `scheme=https&device=desktop&related=true&st=product&q=${QUERY}&start=${OFFSET}&rows=200&safe_search=false&source=search`
        },
        query: `
          query SearchProductQueryV4($params: String!) {
            ace_search_product_v4(params: $params) {
              header {
                totalData
                totalDataText
                processTime
                responseCode
                errorMessage
                additionalParams
                keywordProcess
                __typename
              }
              data {
                isQuerySafe
                ticker {
                  text
                  query
                  typeId
                  __typename
                }
                redirection {
                  redirectUrl
                  departmentId
                  __typename
                }
                related {
                  relatedKeyword
                  otherRelated {
                    keyword
                    url
                    __typename
                  }
                  __typename
                }
                suggestion {
                  currentKeyword
                  suggestion
                  suggestionCount
                  instead
                  insteadCount
                  query
                  text
                  __typename
                }
                products {
                  id
                  name
                  ads {
                    id
                    productClickUrl
                    productWishlistUrl
                    productViewUrl
                    __typename
                  }
                  badges {
                    title
                    imageUrl
                    show
                    __typename
                  }
                  category: departmentId
                  categoryBreadcrumb
                  categoryId
                  categoryName
                  countReview
                  discountPercentage
                  gaKey
                  imageUrl
                  labelGroups {
                    position
                    title
                    type
                    __typename
                  }
                  originalPrice
                  price
                  priceRange
                  rating
                  shop {
                    id
                    name
                    url
                    city
                    isOfficial
                    isPowerBadge
                    __typename
                  }
                  url
                  wishlist
                  __typename
                }
                __typename
              }
              __typename
            }
          }
      `
      }
    })
    let data = response.data
    let products = data.data.ace_search_product_v4.data.products

    // Escapes

    //Unpack

    //Send Log
    fnLog({
      type: 'items',
      items: products.length
    })

    //Ending
    return {
      data: products,
      success: true
    }
  } catch (err) {
    //Retry
    await wait(5000)
    return await page(QUERY, OFFSET, fnLog)
  }
}

export async function fnControl(config) {
  cycleState.status = config.status
}

export default scrape

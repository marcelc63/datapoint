import axios from 'axios'
import _ from 'lodash'

const cycleState = {
  status: 'stop',
  retry: 0
}

async function wait(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms)
  })
}

export async function scrape(CONFIG, fnLog) {
  //Send Log
  fnLog({
    type: 'status',
    status: 'scraping'
  })

  // Define
  let { listings } = CONFIG
  let LISTINGS = listings
  let COUNT = 0
  let MAX = LISTINGS.length

  // let LISTINGS = listings.filter((x) => x['listing.reviews_count'] !== 0)
  console.log('Fetching Reviews: ', LISTINGS.length)

  //Send Log
  fnLog({
    type: 'progress max',
    progressMax: LISTINGS.length
  })

  let result = await cycle(LISTINGS, COUNT, MAX, fnLog)
  console.log('COMPLETE', result)
  return {
    data: result.data,
    success: true
  }
}

async function cycle(LISTINGS, COUNT, MAX, fnLog) {
  console.log('START', COUNT)
  let LISTING = LISTINGS[COUNT]

  console.log(COUNT, 'FIRST')
  // let COUNTER = 0;
  let LISTINGID = LISTING.listingid
  let OFFSET = 0
  let COUNTER = 0

  console.log('OFFSET', OFFSET)

  let reviews = await page(LISTING, LISTINGID, OFFSET, COUNTER, fnLog)
  COUNT = COUNT + 1
  console.log('NEXT LIST', COUNT)

  if (cycleState.status === 'stop') {
    console.log('CYCLE STOP')
    return {
      success: true,
      data: reviews.data
    }
  }

  //Send Log
  fnLog({
    type: 'progress',
    progress: 1
  })

  if (COUNT < MAX) {
    // console.log(reviews);
    let addition = await cycle(LISTINGS, COUNT, MAX, fnLog)
    console.log('CYCLE COMBINE', reviews, addition)
    // let result = reviews.reviews.concat(addition);
    // console.log("TOTAL", result.length);
    return {
      success: true,
      data: reviews.data.concat(addition.data)
    }
  } else if (COUNT >= MAX) {
    console.log('CYCLE DONE')
    cycleState.status = 'stop'
    cycleState.retry = 0
    return {
      success: true,
      data: reviews.data
    }
  }
}

async function page(LISTING, LISTINGID, OFFSET, COUNTER, fnLog) {
  try {
    let SCRAPE_URL = `https://www.airbnb.ae/api/v2/homes_pdp_reviews?currency=USD&key=d306zoyjsyarp7ifhu67rjxn52tv0t20&locale=en&listing_id=${LISTINGID}&_format=for_p3&limit=45&offset=${OFFSET}&order=language_country`
    const response = await axios.get(`${SCRAPE_URL}`)
    console.log(SCRAPE_URL)
    console.log(response)

    let data = response.data
    let payload = {
      success: true,
      count: data.metadata.reviews_count,
      data: data.reviews.map((x) => {
        return {
          ...x,
          stayInfo: {
            id: LISTING.listingid
          }
        }
      })
    }

    console.log('CHECK STATUS', response.status, COUNTER)
    if (response.status === 200) {
      let INCREMENT = 45
      COUNTER = COUNTER + payload.data.length
      console.log('NEXT PAGE', COUNTER, payload.data.length, payload.count)

      //Send Log
      console.log(payload.data.length)
      fnLog({
        type: 'items',
        items: payload.data.length
      })

      if (COUNTER < payload.count) {
        OFFSET = OFFSET + INCREMENT
        // console.log("LENGTH", COUNTER, reviews.count, reviews.reviews.length);
        console.log('CONTINUE', COUNTER, OFFSET)
        let addition = await page(LISTING, LISTINGID, OFFSET, COUNTER, fnLog)

        return {
          ...payload,
          data: payload.data.concat(addition.data)
        }
      } else if (COUNTER >= payload.count) {
        COUNTER = 0
        console.log('DONE', payload.count)
        return payload
      }
    } else {
      console.log('TIMEOUT')
      return payload
    }
  } catch (error) {
    console.log('RETRY PAGE', error.response, cycleState.retry)
    if (error.response.status === 400) {
      // Escape Error
      return {
        success: false,
        data: []
      }
    }

    //Maximum Retry
    cycleState.retry = cycleState.retry + 1
    if (cycleState.retry > 5) {
      return {
        success: false,
        data: []
      }
    }

    await wait(5000)
    let addition = await page(LISTING, LISTINGID, OFFSET, COUNTER, fnLog)
    console.log('AFTER RETRY', addition)
    return {
      success: true,
      count: addition.count,
      data: addition.data
    }
  }
}

export async function fnControl(config) {
  console.log('test', cycleState.status)
  cycleState.status = config.status
  console.log('test', cycleState.status)
}

export default scrape

import axios from 'axios'
import _ from 'lodash'

const cycleState = {
  status: 'stop'
}

async function wait(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms)
  })
}

export async function scrape(CONFIG, fnLog) {
  //Send Log
  fnLog({
    type: 'status',
    status: 'scraping'
  })

  //Define
  let ITEMS = []
  let COUNTER = 0
  let MAX = 0

  //Send Log
  fnLog({
    type: 'progress max',
    progressMax: MAX
  })

  let result = await cycle(/* Params */ fnLog)
  console.log('Scrape complete')
  return {
    data: result.data,
    success: true
  }
}

async function cycle(/* Params */ fnLog) {
  //Define
  let {} = CONFIG

  let result = await page(/* Params */ fnLog)
  if (result.success) {
    if (cycleState.status === 'stop') {
      //Send Log
      fnLog({
        type: 'status',
        status: 'stopping'
      })
      return {
        data: result.data,
        success: true
      }
    }

    //Send Log
    fnLog({
      type: 'progress',
      progress: 1
    })

    if (/* Params */) {
      //Next Page Evaluator
      let data = await cycle(/* Params */ fnLog)
      console.log('CYCLE RESOLVE DATA', data)
      return {
        data: result.data.concat(data.data),
        success: true
      }
    } else {
      //Complete
      console.log('DONE', result.length, OFFSET)
      cycleState.status = 'stop'
      // return result
      return {
        data: result.data,
        success: true
      }
    }
  } else {
    await wait(5000)
    let result = await cycle(/* Params */ fnLog)
    return {
      data: result.data,
      success: true
    }
  }
}

async function page(/* Params */ fnLog) {
  try {
    //Request
    let SCRAPE_URL = ``
    const response = await axios.get(`${SCRAPE_URL}`)
    let data = response.data

    // Escapes
    if (/* Params */) {
      return {
        data: [],
        success: true
      }
    }

    //Unpack

    //Send Log
    fnLog({
      type: 'items',
      items: x.length
    })

    if (/* Params */) {
      //Next Page Evaluate
      let result = await page(/* Params */ fnLog)
      return {
        data: result.data.concat(data.data), //concat response with next response
        success: true
      }
    } else {
      //Ending
      return {
        data: data.data,
        success: true
      }
    }
  } catch (err) {
    //Retry
    await wait(5000)
    return await page(/* Params */ fnLog)
  }
}

export async function fnControl(config) {
  cycleState.status = config.status
}

export default scrape

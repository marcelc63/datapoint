import axios from 'axios'
import _ from 'lodash'

const cycleState = {
  status: 'stop'
}

async function wait(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms)
  })
}

export async function scrape(CONFIG, fnLog) {
  //Send Log
  fnLog({
    type: 'status',
    status: 'scraping'
  })

  //Define
  let PRODUCTS = CONFIG.products
  let COUNT = 0
  let MAX = PRODUCTS.length

  //Send Log
  fnLog({
    type: 'progress max',
    progressMax: MAX
  })

  let result = await cycle(PRODUCTS, COUNT, MAX, fnLog)
  console.log('Scrape complete')
  return {
    data: result.data,
    success: true
  }
}

async function cycle(PRODUCTS, COUNT, MAX, fnLog) {
  //Define
  let PRODUCT = PRODUCTS[COUNT]
  let PRODUCTID = PRODUCT.productid
  let OFFSET = 0

  //Get Data
  let result = await page(PRODUCTID, OFFSET, fnLog)
  if (result.success) {
    if (cycleState.status === 'stop') {
      //Send Log
      fnLog({
        type: 'status',
        status: 'stopping'
      })
      return {
        data: result.data,
        success: true
      }
    }

    //Send Log
    fnLog({
      type: 'progress',
      progress: 1
    })

    if (COUNT < MAX) {
      //Next Page Evaluator
      COUNT = COUNT + 1
      let data = await cycle(PRODUCTS, COUNT, MAX, fnLog)
      console.log('CYCLE RESOLVE DATA', data)
      return {
        data: result.data.concat(data.data),
        success: true
      }
    } else {
      //Complete
      console.log('DONE', result.length, OFFSET)
      cycleState.status = 'stop'
      // return result
      return {
        data: result.data,
        success: true
      }
    }
  } else {
    await wait(5000)
    let result = await cycle(PRODUCTS, COUNT, MAX, fnLog)
    return {
      data: result.data,
      success: true
    }
  }
}

async function page(PRODUCTID, OFFSET, fnLog) {
  try {
    //Request
    const response = await axios({
      url: 'https://gql.tokopedia.com/',
      method: 'post',
      headers: {
        accept: '*/*',
        // 'accept-encoding': 'gzip, deflate, br',
        'accept-language': 'en-US,en;q=0.9,id;q=0.8,ms;q=0.7,pt;q=0.6',
        'content-type': 'application/json',
        // origin: 'https://www.tokopedia.com',
        // 'sec-fetch-dest': 'empty',
        // 'sec-fetch-mode': 'cors',
        // 'sec-fetch-site': 'same-site',
        // 'user-agent':
        //   'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36',
        'x-device': 'desktop-0.0',
        'x-source': 'tokopedia-lite',
        'x-tkpd-lite-service': 'zeus'
      },
      data: {
        operationName: 'PDPReviewListQuery',
        variables: {
          page: OFFSET,
          rating: 0,
          withAttachment: 0,
          productID: parseInt(PRODUCTID),
          perPage: 10
        },
        query: `
          query PDPReviewListQuery($productID: Int!, $page: Int!, $perPage: Int!, $rating: Int!, $withAttachment: Int!) {
            ProductReviewListQuery(productId: $productID, page: $page, perPage: $perPage, rating: $rating, withAttachment: $withAttachment) {
              shop {
                shopId
                name
                image
                url
                __typename
              }
              list {
                reviewId
                message
                productRating
                reviewCreateTime
                reviewCreateTimestamp
                isReportable
                isAnonymous
                imageAttachments {
                  attachmentId
                  imageUrl
                  imageThumbnailUrl
                  __typename
                }
                reviewResponse {
                  message
                  createTime
                  __typename
                }
                likeDislike {
                  totalLike
                  likeStatus
                  __typename
                }
                user {
                  userId
                  fullName
                  image
                  url
                  __typename
                }
                __typename
              }
              __typename
            }
          }
      `
      }
    })

    let data = response.data

    // Escapes

    //Unpack
    let reviews = data.data.ProductReviewListQuery.list

    //Send Log
    fnLog({
      type: 'items',
      items: reviews.length
    })

    if (reviews.length > 0) {
      //Next Page Evaluate
      OFFSET = OFFSET + 1
      let result = await page(PRODUCTID, OFFSET, fnLog)
      return {
        data: result.data.concat(reviews), //concat response with next response
        success: true
      }
    } else {
      //Ending
      return {
        data: [],
        success: true
      }
    }
  } catch (err) {
    //Retry
    await wait(5000)
    return await page(PRODUCTID, OFFSET, fnLog)
  }
}

export async function fnControl(config) {
  cycleState.status = config.status
}

export default scrape

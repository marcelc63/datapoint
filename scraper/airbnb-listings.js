import axios from 'axios'
import _ from 'lodash'

const cycleState = {
  status: 'stop'
}

async function wait(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms)
  })
}

export async function scrape(CONFIG, fnLog) {
  //Send Log
  fnLog({
    type: 'status',
    status: 'scraping'
  })

  //Var
  let INCREMENT = CONFIG.increment
  let ne_lat = CONFIG.ne_lat
  let ne_lng = CONFIG.ne_lng
  let sw_lat = CONFIG.sw_lat
  let sw_lng = CONFIG.sw_lng
  let LONGLAT = {
    ne_lat,
    ne_lng,
    sw_lat,
    sw_lng
  }

  let ENDING = {
    lat: CONFIG.end_lat,
    lng: CONFIG.end_lng
  }
  let OFFSET = {
    lat: 0,
    lng: 0
  }
  let MULTIPLIER = {
    lat: Math.abs(LONGLAT.ne_lat - LONGLAT.sw_lat),
    lng: Math.abs(LONGLAT.ne_lng - LONGLAT.sw_lng)
  }
  let LIMIT = {
    lat: Math.abs(Math.floor((ENDING.lat - LONGLAT.sw_lat) / MULTIPLIER.lat)),
    lng: Math.abs(Math.floor((ENDING.lng - LONGLAT.ne_lng) / MULTIPLIER.lng))
  }

  let PAYLOAD = {
    INCREMENT,
    LONGLAT,
    OFFSET,
    MULTIPLIER,
    LIMIT,
    ne_lat,
    ne_lng,
    sw_lat,
    sw_lng
  }
  console.log(PAYLOAD)

  //Send Log
  fnLog({
    type: 'progress max',
    progressMax: (LIMIT.lat + 1) * (LIMIT.lng + 1)
  })

  let result = await cycle(PAYLOAD, fnLog)
  console.log('Scrape complete')
  return {
    data: _.uniqBy(result.data, (x) => {
      return x.listing.id
    }),
    success: true
  }
}

async function cycle(CONFIG, fnLog) {
  let {
    INCREMENT,
    LONGLAT,
    OFFSET,
    MULTIPLIER,
    LIMIT,
    ne_lat,
    ne_lng,
    sw_lat,
    sw_lng
  } = CONFIG

  LONGLAT.ne_lat = ne_lat - MULTIPLIER.lat * OFFSET.lat
  LONGLAT.ne_lng = ne_lng + MULTIPLIER.lng * OFFSET.lng
  LONGLAT.sw_lat = sw_lat - MULTIPLIER.lat * OFFSET.lat
  LONGLAT.sw_lng = sw_lng + MULTIPLIER.lng * OFFSET.lng
  console.log(LONGLAT.ne_lat)
  let listings = await page(LONGLAT, 0, fnLog)
  if (listings.success) {
    console.log('CYCLE LISTINGS', listings)
    let result = listings.data

    if (cycleState.status === 'stop') {
      console.log('CYCLE STOP')

      //Send Log
      fnLog({
        type: 'status',
        status: 'stopping'
      })

      return {
        data: result,
        success: true
      }
    }

    //Send Log
    fnLog({
      type: 'progress',
      progress: 1
    })

    if (OFFSET.lng < LIMIT.lng && OFFSET.lat <= LIMIT.lat) {
      OFFSET.lng = OFFSET.lng + INCREMENT
      console.log('CYCLE CONTINUE LNG', result.length, OFFSET, LIMIT, LONGLAT)
      //   fnLog(`${result.length}`)
      let data = await cycle(CONFIG, fnLog)
      console.log('CYCLE RESOLVE DATA', data)
      return {
        data: result.concat(data.data),
        success: true
      }
    } else if (OFFSET.lng >= LIMIT.lng && OFFSET.lat < LIMIT.lat) {
      OFFSET.lng = 0
      OFFSET.lat = OFFSET.lat + INCREMENT
      console.log('CYCLE CONTINUE LAT', result.length, OFFSET, LIMIT, LONGLAT)
      let data = await cycle(CONFIG, fnLog)
      console.log('CYCLE RESOLVE DATA', data)
      return {
        data: result.concat(data.data),
        success: true
      }
    } else if (OFFSET.lng === LIMIT.lng && OFFSET.lat === LIMIT.lat) {
      console.log('DONE', result.length, OFFSET)
      cycleState.status = 'stop'
      // return result
      return {
        data: result,
        success: true
      }
    }
  } else {
    await wait(5000)
    let data = await cycle(CONFIG, fnLog)
    return {
      data: data.result,
      success: true
    }
  }
}

async function page(LONGLAT, OFFSET, fnLog) {
  try {
    let SCRAPE_URL = `https://www.airbnb.ae/api/v2/explore_tabs?_format=for_explore_search_web&auto_ib=true&client_session_id=737e7786-e295-4322-a1da-07612a0011ee&currency=USD&current_tab_id=home_tab&experiences_per_grid=20&fetch_filters=true&guidebooks_per_grid=20&has_zero_guest_treatment=true&hide_dates_and_guests_filters=false&is_guided_search=true&is_new_cards_experiment=true&is_standard_search=true&items_per_grid=50&key=d306zoyjsyarp7ifhu67rjxn52tv0t20&locale=en&metadata_only=false&ne_lat=${LONGLAT.ne_lat}&ne_lng=${LONGLAT.ne_lng}&sw_lat=${LONGLAT.sw_lat}&sw_lng=${LONGLAT.sw_lng}&query=jogja&query_understanding_enabled=true&refinement_paths%5B%5D=%2Fhomes&satori_parameters=ERIA&satori_version=1.1.0&screen_height=463&screen_size=large&screen_width=1440&search_by_map=true&search_type=unknown&selected_tab_id=home_tab&show_groupings=true&source=mc_search_bar&supports_for_you_v3=true&timezone_offset=420&version=1.6.2&zoom=16&items_offset=${OFFSET}`

    const response = await axios.get(`${SCRAPE_URL}`)

    console.log(response)

    let data = response.data
    // console.log(data.explore_tabs[0]);
    let explore_tabs =
      data.explore_tabs[0].sections[data.explore_tabs[0].sections.length - 1]

    // Escapes
    if (explore_tabs === undefined) {
      return {
        data: [],
        success: true
      }
    }
    if (explore_tabs.listings === undefined) {
      console.log('PAGE no listings found')
      return {
        data: [],
        success: true
      }
    }

    let listings = explore_tabs.listings
    let has_next_page = data.explore_tabs[0].pagination_metadata.has_next_page
    let items_offset = data.explore_tabs[0].pagination_metadata.items_offset
    console.log(has_next_page, items_offset, listings.length)

    //Send Log
    fnLog({
      type: 'items',
      items: listings.length
    })

    if (has_next_page === true) {
      let data = await page(LONGLAT, items_offset, fnLog)
      let combineData = listings.concat(data.data)
      console.log('PAGE NEXT', combineData)
      return {
        data: combineData,
        success: true
      }
    } else {
      console.log('PAGE END', listings)
      return {
        data: listings,
        success: true
      }
    }
  } catch (err) {
    console.log('PAGE RETRY', err)
    await wait(5000)
    return await page(LONGLAT, OFFSET, fnLog)
  }
}

export async function fnControl(config) {
  console.log('test', cycleState.status)
  cycleState.status = config.status
  console.log('test', cycleState.status)
}

export default scrape

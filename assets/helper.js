import axios from 'axios'

export function flattenObject(ob) {
  var toReturn = {}

  for (var i in ob) {
    if (!ob.hasOwnProperty(i)) continue

    if (typeof ob[i] == 'object' && ob[i] !== null) {
      var flatObject = flattenObject(ob[i])
      for (var x in flatObject) {
        if (!flatObject.hasOwnProperty(x)) continue

        toReturn[i + '.' + x] = flatObject[x]
      }
    } else {
      toReturn[i] = ob[i]
    }
  }
  return toReturn
}

export const replacer = (key, value) =>
  value === null || value === undefined ? '' : value // specify how you want to handle null values here

export async function checkCORS() {
  try {
    let result = await axios.get('https://www.test-cors.org/')
    return true
  } catch (error) {
    return false
  }
}

export async function checkExit(toggle) {
  if (toggle) {
    window.onbeforeunload = function () {
      return 'By leaving you will lose all scraping progress. Are you sure?'
    }
    return
  } else {
    window.onbeforeunload = ''
    return
  }
}

export let datasets = [
  {
    filename: 'Xiaomi - Tokopedia Products Dataset.csv',
    platform: 'Tokopedia',
    datatype: 'Products',
    amount: '2201',
    author: 'marcelc63',
    link:
      'https://drive.google.com/file/d/1FpPplO20sQpZFeXh-dsyU8d4r_qc4upw/view?usp=sharing'
  },
  {
    filename: 'Xiaomi - Tokopedia Reviews Dataset.csv',
    platform: 'Tokopedia',
    datatype: 'Reviews',
    amount: '4086',
    author: 'marcelc63',
    link:
      'https://drive.google.com/file/d/1HZG-ms8FKYBZ3d3NjLT2wcvh_Ui3zbcD/view?usp=sharing'
  },
  {
    filename: 'Oppo - Tokopedia Products Dataset.csv',
    platform: 'Tokopedia',
    datatype: 'Products',
    amount: '21091',
    author: 'marcelc63',
    link:
      'https://drive.google.com/file/d/1AyJMaCebNkQr9sXlTgEBqj1FazDEX1kR/view?usp=sharing'
  },
  {
    filename: 'Bali - Airbnb Listings Dataset.csv',
    platform: 'Airbnb',
    datatype: 'Listings',
    amount: '2301',
    author: 'marcelc63',
    link:
      'https://drive.google.com/file/d/12X3ChVoB26uJFtm4VgetQnilVhDnOFzI/view?usp=sharing'
  },
  {
    filename: 'Bali - Airbnb Reviews Dataset.csv',
    platform: 'Airbnb',
    datatype: 'Reviews',
    amount: '12475',
    author: 'marcelc63',
    link:
      'https://drive.google.com/file/d/1n-e2XVEm3UtzG6UG28AuZc9KoXXt7t8_/view?usp=sharing'
  },
  {
    filename: 'Bandung - Airbnb Listings Dataset.csv',
    platform: 'Airbnb',
    datatype: 'Listings',
    amount: '1044',
    author: 'marcelc63',
    link:
      'https://drive.google.com/file/d/1Ar-XwFmSEo81yvqJL1s6k2jABPjcFZco/view?usp=sharing'
  },
  {
    filename: 'Bandung - Airbnb Reviews Dataset.csv',
    platform: 'Airbnb',
    datatype: 'Reviews',
    amount: '15751',
    author: 'marcelc63',
    link:
      'https://drive.google.com/file/d/1SZ1GtVtvTYK-gMWVmyB9nUBfjdTn7wRt/view?usp=sharing'
  },
  {
    filename: 'Batam - Airbnb Listings Dataset.csv',
    platform: 'Airbnb',
    datatype: 'Listings',
    amount: '398',
    author: 'marcelc63',
    link:
      'https://drive.google.com/file/d/15yen-p2yBd0rVPB5sywrTZFGeOhhqUvh/view?usp=sharing'
  },
  {
    filename: 'Batam - Airbnb Reviews Dataset.csv',
    platform: 'Airbnb',
    datatype: 'Reviews',
    amount: '2742',
    author: 'marcelc63',
    link:
      'https://drive.google.com/file/d/12rY8TMoc-pLg7hOlCVXEmOCvOK4P7unm/view?usp=sharing'
  },
  {
    filename: 'Jakarta - Airbnb Listings Dataset.csv',
    platform: 'Airbnb',
    datatype: 'Listings',
    amount: '1448',
    author: 'marcelc63',
    link:
      'https://drive.google.com/file/d/1WbmNW3iZiD38U8_E_GlBJW4d6N0RklF0/view?usp=sharing'
  },
  {
    filename: 'Jakarta - Airbnb Reviews Dataset.csv',
    platform: 'Airbnb',
    datatype: 'Reviews',
    amount: '17963',
    author: 'marcelc63',
    link:
      'https://drive.google.com/file/d/1FsYlg-0UPZk1e_YGhIWbJQUoA74Kve58/view?usp=sharing'
  },
  {
    filename: 'Makassar - Airbnb Listings Dataset.csv',
    platform: 'Airbnb',
    datatype: 'Listings',
    amount: '159',
    author: 'marcelc63',
    link:
      'https://drive.google.com/file/d/1l1W8v5qpckLsIQyJD9AEv4g8fIeLBRdk/view?usp=sharing'
  },
  {
    filename: 'Makassar - Airbnb Reviews Dataset.csv',
    platform: 'Airbnb',
    datatype: 'Reviews',
    amount: '248',
    author: 'marcelc63',
    link:
      'https://drive.google.com/file/d/1VD5A_lzzs5H4dPgavpnTnDhu-vCYx5Nw/view?usp=sharing'
  },
  {
    filename: 'Mataram - Airbnb Listings Dataset.csv',
    platform: 'Airbnb',
    datatype: 'Listings',
    amount: '1299',
    author: 'marcelc63',
    link:
      'https://drive.google.com/file/d/1wCqF38Ael57zTRJqmA47Cnjow37KkTJ2/view?usp=sharing'
  },
  {
    filename: 'Mataram - Airbnb Reviews Dataset.csv',
    platform: 'Airbnb',
    datatype: 'Reviews',
    amount: '9332',
    author: 'marcelc63',
    link:
      'https://drive.google.com/file/d/18PMa2AtzDZ_ILkJG8Q0Bj_t4ZxlWhP6z/view?usp=sharing'
  },
  {
    filename: 'Medan - Airbnb Listings Dataset.csv',
    platform: 'Airbnb',
    datatype: 'Listings',
    amount: '219',
    author: 'marcelc63',
    link:
      'https://drive.google.com/file/d/1swEw-ms7SIuam_9lWzcuXPGsnGf0fmHK/view?usp=sharing'
  },
  {
    filename: 'Medan - Airbnb Reviews Dataset.csv',
    platform: 'Airbnb',
    datatype: 'Reviews',
    amount: '594',
    author: 'marcelc63',
    link:
      'https://drive.google.com/file/d/10u0h4D9OPL8HARzcpU8ex4x65TzoNXGM/view?usp=sharing'
  },
  {
    filename: 'Semarang - Airbnb Listings Dataset.csv',
    platform: 'Airbnb',
    datatype: 'Listings',
    amount: '319',
    author: 'marcelc63',
    link:
      'https://drive.google.com/file/d/1jNqOWVr4ir_Y6Nvzy5_Iz_BIUlJtXGiP/view?usp=sharing'
  },
  {
    filename: 'Semarang - Airbnb Reviews Dataset.csv',
    platform: 'Airbnb',
    datatype: 'Reviews',
    amount: '2059',
    author: 'marcelc63',
    link:
      'https://drive.google.com/file/d/1R6-NJvHV1-qydUexN17IayTzR5OVUy2Y/view?usp=sharing'
  },
  {
    filename: 'Surabaya - Airbnb Listings Dataset.csv',
    platform: 'Airbnb',
    datatype: 'Listings',
    amount: '468',
    author: 'marcelc63',
    link:
      'https://drive.google.com/file/d/1BTKJ_hLgdJP-gkRVzyMaIqYl3lbnGUAR/view?usp=sharing'
  },
  {
    filename: 'Surabaya - Airbnb Reviews Dataset.csv',
    platform: 'Airbnb',
    datatype: 'Reviews',
    amount: '1829',
    author: 'marcelc63',
    link:
      'https://drive.google.com/file/d/1WAQmOuzPM2yHpgOXSYUzXa_0-_FW9yeh/view?usp=sharing'
  },
  {
    filename: 'Yogyakarta - Airbnb Listings Dataset.csv',
    platform: 'Airbnb',
    datatype: 'Listings',
    amount: '1000',
    author: 'marcelc63',
    link:
      'https://drive.google.com/file/d/1ahEY6w4egBuZDiqTqwJexKOAv6ZJVzqx/view?usp=sharing'
  },
  {
    filename: 'Yogyakarta - Airbnb Reviews Dataset.csv',
    platform: 'Airbnb',
    datatype: 'Reviews',
    amount: '3637',
    author: 'marcelc63',
    link:
      'https://drive.google.com/file/d/1QmtZjYifgHYGe1io2_dtNeVRGsr98pCv/view?usp=sharing'
  }
]
